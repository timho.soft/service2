#FROM openjdk:8-jdk-alpine
#RUN addgroup -S spring && adduser -S spring -G spring
#USER spring:spring
#ARG DEPENDENCY=target/dependency
#COPY ${DEPENDENCY}/BOOT-INF/lib /app/lib
#COPY ${DEPENDENCY}/META-INF /app/META-INF
#COPY ${DEPENDENCY}/BOOT-INF/classes /app
#ENTRYPOINT ["java","-cp","app:app/lib/*","hello.Application"]



FROM openjdk:8u212-jre-alpine3.9
#ARG HTTP_PROXY
#ENV http_proxy=${HTTP_PROXY}
#ENV https_proxy=${HTTP_PROXY}
RUN apk add --no-cache jattach --repository http://dl-cdn.alpinelinux.org/alpine/edge/community/
EXPOSE 8081
COPY "/target/spring-boot-docker-complete-0.0.1-SNAPSHOT.jar" "/target/spring-boot-docker-complete-0.0.1-SNAPSHOT.jar"
#ENTRYPOINT ["java","-cp","app:app/lib/*","hello.Application"]
ENTRYPOINT ["java","-XX:+UseG1GC","-XX:+UseStringDeduplication","-Xms512m","-Xmx4g","-jar","/target/spring-boot-docker-complete-0.0.1-SNAPSHOT.jar"]
#CMD ["--spring.profiles.active=docker"]
