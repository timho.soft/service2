package hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Application {

	@RequestMapping("/")
	public String home() throws InterruptedException {
		long start = System.currentTimeMillis();

		Thread.sleep(7000);

		System.out.println("###### Sleep time in ms = "+(System.currentTimeMillis()-start));

		return "Hello Docker World from Service 2";
	}

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
